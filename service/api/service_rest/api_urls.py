from django.urls import path
from .views import show_technicians, show_specific_technicician, get_appointments, get_specific_appointment, cancel_appointment, finish_appointment

urlpatterns = [
  path('technicians/', show_technicians, name='show_technicians'),
  path('technicians/<int:id>', show_specific_technicician, name='show_specific_technician'),
  path('appointments/', get_appointments, name='get_appointments'),
  path('appointments/<int:id>', get_specific_appointment, name='get_specific_appointment'),
  path('appointments/<int:id>/cancel', cancel_appointment, name='cancel_appointment'),
  path('appointments/<int:id>/finish', finish_appointment, name='finish_appointment')

]