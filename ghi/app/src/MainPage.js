import { Fragment } from "react";

function MainPage() {
  let images = [
    "https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=compress&cs=tinysrgb&w=800",
    "https://images.pexels.com/photos/2379004/pexels-photo-2379004.jpeg?auto=compress&cs=tinysrgb&w=800",
    "https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&w=800",
    "https://images.pexels.com/photos/2691608/pexels-photo-2691608.jpeg?auto=compress&cs=tinysrgb&w=800",
    "https://images.pexels.com/photos/3756927/pexels-photo-3756927.jpeg?auto=compress&cs=tinysrgb&w=800",
    ,
  ];
  return (
    <Fragment>
      <div className="holder">
        <div className="text-center caro-box w-100">
          <div
            id="carouselExampleIndicators"
            class="carousel slide w-100"
            data-bs-ride="carousel"
          >
            <ol class="carousel-indicators">
              <li
                data-bs-target="#carouselExampleIndicators"
                data-bs-slide-to="0"
                class="active"
              ></li>
              <li
                data-bs-target="#carouselExampleIndicators"
                data-bs-slide-to="1"
              ></li>
              <li
                data-bs-target="#carouselExampleIndicators"
                data-bs-slide-to="2"
              ></li>
            </ol>
            <div class="carousel-inner w-100">
              <div class="carousel-item active carosel-img">
                <img
                  class="d-block w-100 carosel-img"
                  src="https://images.pexels.com/photos/2920064/pexels-photo-2920064.jpeg?auto=compress&cs=tinysrgb&w=800"
                  alt="First slide"
                />
              </div>
              <div class="carousel-item carosel-img">
                <img
                  class="d-block w-100 carosel-img"
                  src="https://images.pexels.com/photos/10292236/pexels-photo-10292236.jpeg?auto=compress&cs=tinysrgb&w=800"
                  alt="Second slide"
                />
              </div>
              <div class="carousel-item carosel-img">
                <img
                  class="d-block carosel-img w-100"
                  src="https://images.pexels.com/photos/5288731/pexels-photo-5288731.jpeg?auto=compress&cs=tinysrgb&w=800"
                  alt="Third slide"
                />
              </div>
            </div>
          </div>
        </div>
        <div>
          <h2 className="slogan">
            As masters of automobiles, whatever you need we got it!
          </h2>
        </div>
      </div>
      <div className="container holder">
        <div className="mx-auto testimony-all">
          {images.map((image) => {
            return (
              <div className="testimony-img-holder">
                <img src={image} alt="..." className="testimony-img" />
              </div>
            );
          })}
          <div className="mx-3 test-text">Over 10,000 5 star testimonies!</div>
        </div>
      </div>
      <div class="container car-container shadow">
        <div class="row text-center">
          <div class="col car-col">
            <div className="car-box">
              <div className="car-box2">
                <img
                  src="../images/jeep-removebg-preview (1).png"
                  alt="..."
                  className="car-img"
                />
              </div>
            </div>
          </div>
          <div class="col car-col car-text">
            "Incredible car service! Treated like royalty, <br /> perfect
            condition, complimentary wash. <br /> Highly recommend!" <br />-
            Barbara
          </div>
        </div>
        <div class="row text-center">
          <div class="col car-col car-text text-center">
            "Wonderful experience!
            <br /> Knowledgeable mechanics, <br /> quick fix, great prices.
            <br />
            Left feeling confident!" <br /> -Jason
          </div>
          <div class="col car-col">
            <div className="car-box">
              <div className="car-box2">
                <img
                  src="../images/honda2-removebg-preview (1).png"
                  alt="..."
                  className="car-img"
                />
              </div>
            </div>
          </div>
        </div>
        <div class="row text-center">
          <div class="col car-col">
            <div className="car-box">
              <div className="car-box2">
                <img
                  src="../images/toyota-removebg-preview.png"
                  alt="..."
                  className="car-img"
                />
              </div>
            </div>
          </div>
          <div class="col car-col car-text">
            "Hassle-free maintenance! <br /> Fantastic team, took care of
            everything, <br /> provided useful tips. Customer for life!"
          </div>
        </div>
      </div>
      <div class="text-center mt-10 container2 shadow">
        <h1 className="text-center heading">Our technicians</h1>
        <div class="row">
          <div class="col">
            <div class="card">
              <img
                src="https://images.pexels.com/photos/3806249/pexels-photo-3806249.jpeg?auto=compress&cs=tinysrgb&w=800"
                class="card-img-top"
                alt="..."
              />
              <div class="card-body">
                <p class="card-text">
                  Speed and efficiency is important to us. We want to fix your
                  car as fast as we can to the best of our ability without you
                  having to wait all day.
                </p>
              </div>
            </div>
          </div>
          <div class="col text-center">
            <div class="card shadow-sm">
              <img
                src="https://images.pexels.com/photos/162553/keys-workshop-mechanic-tools-162553.jpeg?auto=compress&cs=tinysrgb&w=800"
                class="card-img-top"
                alt="..."
              />
              <div class="card-body">
                <p class="card-text">
                  We have the tools, certifications, and time of day for any
                  customer issue. We are ready to fix whatever you bring to us,
                  so bring it on in!
                </p>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card shadow-sm">
              <img
                src="https://images.pexels.com/photos/2244746/pexels-photo-2244746.jpeg?auto=compress&cs=tinysrgb&w=800"
                class="card-img-top"
                alt="..."
              />
              <div class="card-body">
                <p class="card-text">
                  We have technicians specialized in all areas of the field. We
                  have quick fix technicians, engine technicians and
                  transmission technicians.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
        <div class="col-md-4 d-flex align-items-center">
          <span class="text-muted">© 2023 Dealership, Inc</span>
        </div>
        <ul class="nav col-md-2 list-unstyled d-flex">
          <li class="ms-3 foot-link">
            <a
              class="text-muted"
              href="https://www.linkedin.com/in/christopher-bustamante-6318a91b9/"
            >
              <i class="bi bi-linkedin"></i>
            </a>
          </li>
          <li class="ms-3 foot-link mb-2">
            <a href="https://gitlab.com/bustamantechris99">
              <img
                src="../images/gitlab-logo-500.png"
                alt=""
                className="git foot-link"
              />
            </a>
          </li>
        </ul>
      </footer>
    </Fragment>
  );
}

export default MainPage;
