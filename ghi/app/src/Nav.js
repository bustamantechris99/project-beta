import React from "react";
import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light">
      <div className="container-fluid">
        <h1 className="navbar-brand logo" to="">
          Vroom{" "}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            class="bi bi-car-front"
            viewBox="0 0 16 16"
          >
            <path d="M4 9a1 1 0 1 1-2 0 1 1 0 0 1 2 0Zm10 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0ZM6 8a1 1 0 0 0 0 2h4a1 1 0 1 0 0-2H6ZM4.862 4.276 3.906 6.19a.51.51 0 0 0 .497.731c.91-.073 2.35-.17 3.597-.17 1.247 0 2.688.097 3.597.17a.51.51 0 0 0 .497-.731l-.956-1.913A.5.5 0 0 0 10.691 4H5.309a.5.5 0 0 0-.447.276Z" />
            <path d="M2.52 3.515A2.5 2.5 0 0 1 4.82 2h6.362c1 0 1.904.596 2.298 1.515l.792 1.848c.075.175.21.319.38.404.5.25.855.715.965 1.262l.335 1.679c.033.161.049.325.049.49v.413c0 .814-.39 1.543-1 1.997V13.5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-1.338c-1.292.048-2.745.088-4 .088s-2.708-.04-4-.088V13.5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-1.892c-.61-.454-1-1.183-1-1.997v-.413a2.5 2.5 0 0 1 .049-.49l.335-1.68c.11-.546.465-1.012.964-1.261a.807.807 0 0 0 .381-.404l.792-1.848ZM4.82 3a1.5 1.5 0 0 0-1.379.91l-.792 1.847a1.8 1.8 0 0 1-.853.904.807.807 0 0 0-.43.564L1.03 8.904a1.5 1.5 0 0 0-.03.294v.413c0 .796.62 1.448 1.408 1.484 1.555.07 3.786.155 5.592.155 1.806 0 4.037-.084 5.592-.155A1.479 1.479 0 0 0 15 9.611v-.413c0-.099-.01-.197-.03-.294l-.335-1.68a.807.807 0 0 0-.43-.563 1.807 1.807 0 0 1-.853-.904l-.792-1.848A1.5 1.5 0 0 0 11.18 3H4.82Z" />
          </svg>
        </h1>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/">
                Home
              </NavLink>
            </li>
            <div className="dropdown nav-item">
              <button
                className="btn dropdown-toggle nav-item"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/customers/create/">
                    Add a Customer
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/salespeople/create">
                    Add a Salesperson
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/sales/create">
                    Record a Sale
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/salespeople">
                    List of Salespeople
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/customers">
                    List of Customers
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/sales">
                    List of Sales
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/sales/history">
                    List of Salesperson History
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown nav-item">
              <button
                className="btn dropdown-toggle nav-item"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Services
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/technicians">
                    Technician list
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/technicians/create">
                    Create a technician
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/appointments/create">
                    Create an Appointment
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/appointments/history">
                    View Service History
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="/appointments/">
                    Service Appointments
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown nav-item">
              <button
                className="btn dropdown-toggle nav-item"
                type="button"
                id="dropdownMenuButton1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="manufacturers/create">
                    Create Manufacturer
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="manufacturers">
                    Manufacturers List
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="models/create">
                    Create A Vehicle Model
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="models">
                    Models List
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="automobiles">
                    Automobile List
                  </NavLink>
                </li>
                <li className="dropdown-item nav-item">
                  <NavLink className="nav-link" to="automobiles/create">
                    Automobile Form
                  </NavLink>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
