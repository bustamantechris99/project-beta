from django.urls import path
from .views import (automobile_list, sales_list, api_sales, customer_list, api_salespeople)


urlpatterns = [
    path('automobile/', automobile_list, name='automobile_list'),
    path('sales/', sales_list, name = 'sales_list'),
    path('sale/<int:id>/', api_sales, name = 'api_sales'),
    path('customers/', customer_list, name = 'customer_list'),
    path('salespeople/', api_salespeople, name = 'api_salespeople'),
]