from django.db import models


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.first_name


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=500)
    phone_number = models.CharField(max_length=11, unique=True)

    def __str__(self):
        return self.first_name


class Sale(models.Model):
    automobile = models.ForeignKey(
        "AutomobileVO",
        related_name="automobile",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        "Salesperson",
        related_name="salesperson",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        "Customer",
        related_name="customerr",
        on_delete=models.PROTECT,
    )
    price = models.CharField(max_length=100)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin
